/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supakit.gameox_oop;

import java.util.Scanner;

/**
 *
 * @author SUPAKIT KONGKAM
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row;
    int col;
    static int count = 0;

    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }

    public void showTable() {
        table.showTable();
    }

    public void showScore() {
        System.out.println("PlayerX " + playerX.getWin() + " - "
                + playerO.getWin() + " PlayerO");
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            count++;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
            count--;
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void showBye() {
        System.out.println("Bye bye ....");
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    this.showTable();
                    System.out.println("draw...");
                    this.showScore();
                } else {
                    this.showTable();
                    System.out.println("Player " + table.getWinner().getName()
                            + " win....");
                    this.showScore();

                }
                System.out.println("Restart a new game "
                        + "of Exit the game?" + "\nY or N");
                char choose = kb.next().charAt(0);
                if (choose == 'Y' || choose == 'y') {
                    this.newGame();
                    table.clearTableNewGame();
                    this.showWelcome();
                } else {
                    if (playerX.getWin() > playerO.getWin()) {
                        System.out.println("Game over!!!\nPlayer X win...");
                    } else if (playerX.getWin() < playerO.getWin()) {
                        System.out.println("Game over!!!\nPlayer O win...");
                    } else {
                        System.out.println("Game over!!!\nDrawww!!!");
                    }
                    this.showBye();
                    break;
                }

            }
            table.switchPlayer();
        }

    }

}
